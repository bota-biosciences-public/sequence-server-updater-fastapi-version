# Deploying SequenceServer on Azure and Integration with Benchling  – A Complete Guide 


- [Deploying SequenceServer on Azure and Integration with Benchling  – A Complete Guide](#deploying-sequenceserver-on-azure-and-integration-with-benchling---a-complete-guide)
  - [About This Guide](#about-this-guide)
  - [What is SequenceServer](#what-is-sequenceserver)
  - [Install and Run SequenceServer on a Local PC / Server](#install-and-run-sequenceserver-on-a-local-pc--server)
    - [Gem Package Method (Not Recommended):](#gem-package-method-not-recommended)
    - [Docker Method (Recommended):](#docker-method-recommended)
  - [Challenges of Deploying SequenceServer on Azure App Service](#challenges-of-deploying-sequenceserver-on-azure-app-service)
  - [Deploy SequenceServer on Azure App Service](#deploy-sequenceserver-on-azure-app-service)
    - [Step 1. Set up Azure Container Registry](#step-1-set-up-azure-container-registry)
    - [Step 2. Set up Azure File Shares](#step-2-set-up-azure-file-shares)
    - [Step 3. Build Docker Image for SequenceServer Database Updater](#step-3-build-docker-image-for-sequenceserver-database-updater)
    - [Step 4. Deploy SequenceServer Database Updater App on Azure App Services](#step-4-deploy-sequenceserver-database-updater-app-on-azure-app-services)
    - [Step 5. (Optional) Build Docker Image for SequenceServer](#step-5-optional-build-docker-image-for-sequenceserver)
    - [Step 6. Deploy SequenceServer on Azure App Service](#step-6-deploy-sequenceserver-on-azure-app-service)
    - [Step 7. Configure SequenceServer Database Updater App](#step-7-configure-sequenceserver-database-updater-app)
    - [Step 8. Configure File Sharing Between SequenceServer and SequenceServer Database Updater](#step-8-configure-file-sharing-between-sequenceserver-and-sequenceserver-database-updater)
  - [Using SequenceServer Database Updater](#using-sequenceserver-database-updater)
  - [Diagnose SequenceServer Database Updater](#diagnose-sequenceserver-database-updater)
  - [Authentication](#authentication)


## About This Guide

This document is a guide for deploying your own **SequenceServer** (https://sequenceserver.com/), as well as the new **SequenceServer Database Updater app** on Azure App Service. The SequenceServer Database Updater app extends the functionalities of SequenceServer, including: 

- Integrate Benchling and SequenceServer -- perform BLAST searches against the latest data from your Benchling Warehouse, with the easy-to-use web interface of SequenceServer 

- Allow users to upload FASTA files and perform BLAST searches against the files 

- Provide a clickable link on the search result of SequenceServer that redirects to the corresponding page on Benchling 

- Automatically restart the Azure App Service instance hosting SequenceServer, whenever its databases are changed 

![SequenceServer Updater App Main Page](./pics/updater_app_main_page.png "SequenceServer Updater App Main Page")
![SequenceServer Updater App Upload Page](./pics/updater_app_upload.png "SequenceServer Updater App Upload Page")
![Project Diagram](./pics/project_diagram.png "Project Diagram")

**For quick setup, skip to the [Deploy SequenceServer on Azure App Service](#deploy-sequenceserver-on-azure-app-service) section of this guide.**


## What is SequenceServer 

SequenceServer is a free and open-source BLAST server that can be installed and run on any PCs, servers or cloud services. SequenceServer provides an easy-to-use web interface for users to submit BLAST search requests, as well as visualizations for interpreting BLAST results. More information regarding the visualization functionalities of SequenceServer can be found here (https://sequenceserver.com/blog/visualizing_blast_results/) 

![SequenceServer Search Page](./pics/sequenceserver_search_page.png "SequenceServer Search Page")
![SequenceServer Search Results](./pics/sequenceserver_search_result.png "SequenceServer Search Results")


## Install and Run SequenceServer on a Local PC / Server 

You can either install SequenceServer as a Ruby Gem package, or you can use Docker with SequenceServer’s official Dockerfile. By default, SequenceServer uses the /db directory to scan and store BLAST databases. 

 

### Gem Package Method (Not Recommended): 

 

To run SequenceServer as a Ruby Gem package, you must first install the following requirements on your system: 

- Linux or Mac and Ruby (≥ 2.3; preferably ≥ 2.5; Ruby 3 not supported)  

- NCBI BLAST+ (2.10.0+) is interactively downloaded if absent 

- Standard Unix build tools (e.g., gcc, make) are required to install SequenceServer. This is because SequenceServer's need to parse BLAST's XML output compiles some C code as part of the installation process. This means that the On a Mac, this means having XCode and CLI tools for XCode installed. On Ubuntu and other Debian-based Linux systems, you would have to install the ruby-dev and build-essential packages in addition to ruby. 

Then you can install it with: 

`sudo gem install sequenceserver`

To run and config: 

`sequenceserver`


### Docker Method (Recommended): 

While it is possible to run SequenceServer as a Ruby Gem package, it is recommended to run it in a Docker Container instead. The benefits include: 

- Way less dependency issues 

- Easier deployment on cloud services 

- Potential possibility of integration with Docker Compose on regular servers / cloud container services that support Docker-in-Docker (e.g., Amazon Elastic Container Services). More on that later 

To run SequenceServer with the official docker image: 

`docker pull wurmlab/sequenceserver`

`docker run -itp 4567:4567 -v /path-to-database-dir:/db wurmlab/sequenceserver `

- **Replace `/path-to-database-dir` with the full path to any folder on your local machine 	that holds the FASTA files you want to use as the BLAST database.**

- The `–v /path-to-database-dir:/db` parameter mounts your local `/path-to-database-dir` to the `/db` directory inside the container 

Upon running the docker run command, SequenceServer will look for BLAST database files in the `/db` directory. If none of them were found, SequenceServer will launch some command line prompts that ask whether you want to search for FASTA files in `/db`, and guide you to generate BLAST database files with your FASTA files. Just answer `yes` a few times and you are good to go. 

If there are already some BLAST database files present in `/db`, SequenceServer will launch immediately without asking any questions in the command line prompts.  

Now you can open http://localost:4567 in your web browser and start BLAST-ing! 


## Challenges of Deploying SequenceServer on Azure App Service 

<span style="color:red"> [For detailed instructions on deployment, go to [Deploy SequenceServer on Azure App Service](#deploy-sequenceserver-on-azure-app-service) section. You do not have to follow the steps in this section. This section is just for your reference] </span>

It is possible to deploy your own SequenceServer instance on Azure App Service (https://azure.microsoft.com/en-us/services/app-service/) so that everyone in your company/organization can have shared access. Azure App Service supports containerized web apps, so it is very easy to deploy any Docker based web apps, including single-container apps and multi-container apps (Docker-compose).  

However, you may face some challenges when deploying the SequenceServer on Azure App Service, compared to the relatively easy setup on your local computer. This is true for many other similar cloud services like Amazon ECS. For example: 

- **Your Azure App Service running the official SequenceServer image (https://hub.docker.com/r/wurmlab/sequenceserver) will fail to start if there are no BLAST db files in `/db`** 

    - **Why?**  

      - Remember in the last section [Install and Run SequenceServer on a Local PC / Server – Docker Method], we mentioned that SequenceServer will ask your confirmations for searching for FASTA files and building BLAST db files in the command line prompts. Unfortunately, as for now, it is impossible to access the command line interface during the warm-up stage ($ docker run ...) of an Azure App Service.  
    
      -  This is not really a huge problem but can be very confusing if you are trying to deploy SequenceServer on Azure for the first time.  

    <br>

    - **Solution 1 (Manually build and upload BLAST db):** 

        If you don’t want to modify and rebuild the official SequenceServer image, one way to circumvent this problem is to have your BLAST database files ready in the `/db` directory of your App Service’s container.  

        <br> 

        **This solution is great if you don’t need to update your BLAST database very often.**

        <br>
        Just having some FASTA files in there is not enough, since what SequenceServer really 	need are BLAST database files. To make BLAST db files, you would need: 

        - Read https://www.ncbi.nlm.nih.gov/books/NBK569841/  

        - BLAST+ Command Line Applications 

        - Valid FASTA files of your sequences 

        <br>

        Then you can follow the instructions and use the `makeblastdb` command to generate the necessary files for SequenceServer.  
        <br>
        You can either install the BLAST+ Command Line Applications on your local computer or run the SequenceServer docker image locally to help you generate the BLAST database files.  
        <br>
        The result should look like this: 

        ![BLAST db samples](./pics/blast_db_samples.png "BLAST db samples")

        Then you can create an **Azure Storage Account**, upload these files to an **Azure File shares**, and mount it to the `/db` directory of your App Service using **Path mappings** on **Azure Portal**. Restart your App Service, and SequenceServer should start working.  

        ![Azure file share](./pics/azure_file_share.png "Azure file share")
        ![Azure path mappings](./pics/azure_path_mappings.png "Azure path mappings")

    <br>

    - **Solution 2 (Modify SequenceServer)**

        This is not necessary, but if you just want to have SequenceServer running on Azure App Service, even when there are no BLAST database files, you can modify the code of SequenceServer to disable the command line prompts mentioned earlier.  
        <br>

        Download SequenceServer’s source code from https://github.com/wurmlab/sequenceserver , open the `sequenceserver/bin/sequenceserver` file, find the line that contains `rescue SequenceServer::NO_BLAST_DATABASE_FOUND => e`, and disable the following `unless` loop that has the condition `make_blast_databases?`. This is the part of code that asks you to search for FASTA files and generate BLAST db files in the command line when the container is started. Rebuild the image with your modified SequenceServer source code, and the image should work on Azure App Service without any BLAST db files.  

        <br>

    - **Solution 3 (Use SequenceServer Database Updater)**

        Since the SequenceServer Database Updater will perform the steps in **Solution 1** for you 	automatically. Follow the next section for detailed steps.   

## Deploy SequenceServer on Azure App Service 

In this section, we will walk through each step of deploying SequenceServer and SequenceServer Database Updater on Azure App Service. After this guide, you should have: 

- **2 Azure App Service instances** 

  - SequenceServer (**Single Container**) 

    - Modified docker image for integration with Benchling 

      - Or use the official version if you don’t need a clickable link to Benchling in the search result 

  - SequenceServer Database Updater (**Single Container**) 

      - Pulls data from Benchling Warehouse 

      - Restart SequenceServer instance if database is updated 

      - Provide redirection link of sequence ids to Benchling 

- **1 Azure Container Registry** (Or you can use Docker Hub) 

    - For hosting two docker images: 

        - Modified SequenceServer 

        - SequenceServer Database Updater 

- **1 Azure Storage Account** 

  - **1 Azure file share** 

    - For storing FASTA and BLAST db files 

    - Mounted to both of your Azure App Services 

![Project Diagram](./pics/project_diagram.png "Project Diagram")

### Step 1. Set up Azure Container Registry

Go to Azure Portal (https://portal.azure.com) and choose "Container registries". From there, create a new container registry. The URL to your new registry will be `https://the_name_you_chose.azurecr.io` . This is where you will be uploading your docker images to be deployed on Azure App Service. 

![Create ACR](./pics/create_acr.png "Create ACR")

### Step 2. Set up Azure File Shares

Go to **Azure Portal** (https://portal.azure.com) and choose **“Storage accounts”**. Press the "**+ Create**" button. Keep the default settings. 

Once you have created a Storage account, press the name of the Storage account you want to use. 

![Azure Storage Account](./pics/azure_storage_account.png "Azure Storage Account")

From the tabs on the left, choose **“File shares”**. Use the **“十 File share”** button on the right to create a new File share. This is where you will be storing your FASTA and BLAST db files. You will also be mounting this File share to both of your App Services (SequenceServer and SequenceServer Database Updater) using Path Mapping at `/db`. More on that later. 

### Step 3. Build Docker Image for SequenceServer Database Updater 

1. Clone the SequenceServer Database Updater project to your local directory: 

`git clone https://gitlab.com/bota-biosciences-public/sequence-server-updater-fastapi-version.git ` 

1. Build Docker image 

`cd sequence-server-updater `

`docker build -t YOUR_REPOSITORY_NAME.azurecr.io/sequenceserver-updater . `

- **(replace ‘’YOUR_REPOSITORY_NAME” with the name of the Container repository you created earlier)**

3. Log into your Azure Container Repository 

`docker login YOUR_REPOSITORY_NAME.azurecr.io/sequenceserver-updater` 

4. Push your Docker image to your repository 

`docker push YOUR_REPOSITORY_NAME.azurecr.io/sequenceserver-updater` 

Go to Azure Container registries and enter the container registries you created earlier. Choose Services – Repositories from the sidebar. You should see a new Repository called “sequenceserver-updater” on the page. 


### Step 4. Deploy SequenceServer Database Updater App on Azure App Services

Go to Azure Portal (https://portal.azure.com) and choose “App Services”. From there, press “Create”.  

On the "**Basics**" page, give a name to your new App Service that will host your SequenceServer Database Updater App. You won’t be able to change the name afterwards. The URL of your new App Service will be https://yourappname.azurewebsites.net, so choose the name carefully.  

Then, choose “Docker Container” for “Publish” and “Linux” for “Operating System”. 

![Create Azure Web App](./pics/azure_create_webapp.png "Create Azure Web App")
![Create Azure Web App](./pics/azure_create_webapp_docker_tab.png "Create Azure Web App")

On the “**Docker**” tab, choose “**Single Container**” and “**Azure Container Registry**”. Then, choose the Registry and Image name you created earlier for **SequenceServer Database Updater**. Keep other settings default and create the service.  

Wait for a minute, and you should be able to see your new app in the list of your App Services. Click on the name, and you should see the URL of your new app. See if it is working!  

![Azure App Portal](./pics/azure_app_portal.png "Azure App Portal")

### Step 5. (Optional) Build Docker Image for SequenceServer 

This step is optional, since there is already an official Docker image for SequenceServer (wurmlab/sequenceserver) available on Docker Hub, and you can use that image to create an Azure App Service directly.  

However, if you want to: 

- Have a nice clickable link that redirects you to the corresponding page of a sequence on your Benchling Account, on the search result page of SequenceServer 

![SequencServer link to benchling](./pics/sequenceserver_link_to_benchling.png "SequencServer link to benchling")

- AND / OR avoid SequenceServer to fail to start on Azure App Service when there is no FASTA and BLAST db files present 

Then you should modify SequenceServer and build your own SequenceServer Docker image. 

To enable the clickable link to Benchling from the search results page: 

1. Go to the URL of the SequenceServer Database Updater app you deployed earlier. You should see the web interface: 

![Database Updater App](./pics/database_updater_app.png "Database Updater App")

2. Go to the “Benchling Redirect API” tab from the sidebar on the left. You will see the instructions on the page. 

![Redirect Instructions](./pics/updater_redirect_instructions.png "Redirect Instructions")

3. Copy the code snippet from the page. The URL in the code snippet is filled automatically based on the actual URL of your App Service, so you don’t have to change the code snippet at all. 

4. Get the source code of SequenceServer from its official GitHub repository: 

  `git clone https://github.com/wurmlab/sequenceserver.git `

  `cd sequenceserver `

5. Open `sequenceserver/lib/sequenceserver/links.rb` inside the repository 

6. Insert the function in the code snippet into the file. Make sure that the function is on the same indentation level as other existing functions in the file 

7. If you want to prevent SequenceServer from failing to start on Azure when there are no BLAST db files, check “Solution 2” in section *“Challenges of Deploying SequenceServer on Azure App Service”*. Otherwise, this is all you need to do with SequenceServer’s source code! 

8. Now that you have done modifying the SequenceServer’s source code, you can rebuild the Docker image and push it to your Azure Container Registries 


### Step 6. Deploy SequenceServer on Azure App Service 

Since you have already successfully deployed a Docker image (SequenceServer Database Updater) to Azure App Service in Step 4, you should have been somewhat familiar with this process. Repeat the process and create another Azure App Service, but with the SequenceServer image you created in Step 5. Unless you have modified the relevant code in SequenceServer in Step 5, you won’t be able to see its interface yet at this point. But don’t worry, we will fix that later. 


### Step 7. Configure SequenceServer Database Updater App 

Now that you have deployed both App Services for SequenceServer and SequenceServer Database Updater, it is time to configure them to work together nicely!  

From Azure Portal, open the page for your SequenceServer Database Updater app.  

**1. Provide login credentials for your Benchling Warehouse Postgres Database**

  These credentials can be set using environmental variables safely. Go to Settings – Configuration tab from the sidebar: 

![Azure Env Vars](./pics/azure_env_vars.png "Azure Env Vars")

Under “Application Settings”, add three new variables using the “+ New application setting” button 

| Name                        | Value                                                                                     |
|-----------------------------|-------------------------------------------------------------------------------------------|
| BENCHLING_POSTGRES_HOST     | URL to your benchling warehouse. E.g. postgres-warehouse.YOUR_ACCOUNT_NAME.benchling.com  |
| BENCHLING_POSTGRES_ USER    | Username to your Benchling Postgres warehouse                                             |
| BENCHLING_POSTGRES_PASSWORD | Password to your Benchling Postgres warehouse                                             |

<br>

**2. Provide Azure credentials to control your SequenceServer instance** 

Whenever you update or add the BLAST databases to the `/db` directory, you need to restart SequenceServer in order to enable them.  

**SequenceServer Database Updater can automatically restart your App Service running SequenceServer for you.** To do that, you need to generate a secret key to the App Service running SequenceServer: 

- Install Azure Cli tools on any computer 

- Run this command: 

`az ad sp create-for-rbac --role owner --scopes /subscriptions/YOUR_SUBSCRIPTION_ID `

Replace `YOUR_SUBSCRUPTION_ID` with the Subscription ID found on the Azure Portal page of your **SequenceServer** App Service (NOT the one on your SequenceServer Database Updater App!!!).  

Then again, under “Application Settings”, add some new variables sing the “+ New application setting” button 

| Name                    | Value                                                       |
|-------------------------|-------------------------------------------------------------|
| AZURE_SUBSCRIPTION_ID   | Subscription ID of your SequenceServer instance             |
| AZURE_CLIENT_ID         | Generated from Azure Cli                                    |
| AZURE_SECRET            | Generated from Azure Cli                                    |
| AZURE_TENANT            | Generated from Azure Cli                                    |
| AZURE_RESOURCE_GROUP    | Name of the resource group of your SequenceServer instance  |
| AZURE_APP_SERVICE_NAME  | Name of your SequenceServer instance                        |


### Step 8. Configure File Sharing Between SequenceServer and SequenceServer Database Updater 

Last step! Now we just need to enable file sharing between your two App Services, so that when SequenceServer Database Updater generates or updates some BLAST databases, you will be able to use them immediately in SequenceServer. 

**Do the following steps for BOTH the SequenceServer App Service and the SequenceServer Database Updater App Service.**

Go to “**Settings - Configuration**” tab of your App Service 

![Azure path mappings](./pics/azure_path_mappings.png "Azure path mappings")

Under “**Path mappings**”, click “**+ New Azure Storage Mount**". Mount the Azure file share that you created in Step 2 to `/db`. Again, do this for BOTH of your App Services!  


## Using SequenceServer Database Updater 

Congratulations! You have successfully set up the SequenceServer Database Updater and SequenceServer on Azure.  

**If this is your first time using this setup, you will need to have some BLAST database files in your Azure file shares to get SequenceServer running. Open the SequenceServer Database Updater.**

To export your sequences from your Benchling Warehouse, press the “Run Query and Rebuild BLAST DB” button on the main page of SequenceServer Database Updater. Refresh the page to check if the operation is successful. Upon a successful run, you should see the files in your Azure file share. You will also be able to choose the database on your SequenceServer. 

**Press this button whenever you want to pull the latest data from your Benchling Warehouse.** 

![SequenceServer Updater App Main Page](./pics/updater_app_main_page.png "SequenceServer Updater App Main Page")

You can also upload additional FASTA files to be used as BLAST databases in SequenceServer. Simply go to the “Import FASTA” tab in the SequenceServer Updater App and follow the instructions. You will be able to see your new BLAST databases in SequenceServer right away. 

![SequenceServer Updater App Upload Page](./pics/updater_app_upload.png "SequenceServer Updater App Upload Page")

## Diagnose SequenceServer Database Updater 

If you are having trouble using SequenceServer Database Updater, use the “Diagnostic” page to see if any of the environmental variables are not set correctly. Revisit **Step 7** of section [***Deploy SequenceServer on Azure App Service***] to fix the configurations.  

![SequenceServer Updater App Diag Page](./pics/updater_app_diag.png "SequenceServer Updater App Diag Page")

## Authentication 

Don’t forget to add user authentication to both of your Azure App Services (SequenceServer & SequenceServer Database Updater)! You can do that through “**Settings – Authentication**” tab of your App Service page on Azure Portal.  
