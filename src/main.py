from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from . import models
from .database import engine
from .routers import updater, redirect, diagnostic


app = FastAPI()

models.Base.metadata.create_all(engine)

app.mount("/static", StaticFiles(directory="static"), name="static")

app.include_router(updater.router)
app.include_router(redirect.router)
app.include_router(diagnostic.router)
