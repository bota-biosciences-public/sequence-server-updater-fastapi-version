from .database import Base
from sqlalchemy import (
    Column,
    Integer,
    String,
    DateTime,
)
from datetime import datetime


class TaskStatus(Base):
    __tablename__ = "task_status"

    id = Column(Integer, primary_key=True)
    db_status = Column(String)
    cloud_app_status = Column(String, default="Pending")
    datetime = Column(DateTime, default=datetime.now)
