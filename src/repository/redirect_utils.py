from enum import Enum
import psycopg
from ..global_settings import *


class BenchlingDbStatus(str, Enum):
    SUCCESS = "Found Corresponding Benchling URL"
    NO_ENV_VAR = "Environmental Variable For Benchling DB Logins Are Missing"
    CONNECTION_ERROR = "Benchling DB Connection Error"
    QUERY_ERROR = "Benchling DB Query Error"
    URL_NOT_FOUND = "Benchling URL Not Found"
    TABLES_NOT_FOUND = "Failed to find database tables containing \
                        'url$' column in Benchling warehouse"


def get_benchling_url(seq_id: str):
    '''
    Returns the URL of the Benchling page of a sequence.

            Parameters:
                    seq_id (str): The sequence id of the sequence

            Returns:
                    status (str): The status of the query operation
                    url (str): The URL to the Benchling page
    '''

    print("\nConnecting to database...\n")
    print(BENCHLING_POSTGRES_HOST + ":" + str(BENCHLING_POSTGRES_PORT))

    if not BENCHLING_POSTGRES_USER or not BENCHLING_POSTGRES_PASSWORD:
        return BenchlingDbStatus.NO_ENV_VAR, None

    try:
        db_conn = psycopg.connect(host=BENCHLING_POSTGRES_HOST,
                            port=BENCHLING_POSTGRES_PORT,
                            dbname=BENCHLING_POSTGRES_DATABASE,
                            user=BENCHLING_POSTGRES_USER,
                            password=BENCHLING_POSTGRES_PASSWORD)
        db_cur = db_conn.cursor()
    except:
        return BenchlingDbStatus.CONNECTION_ERROR, None

    print("\nDatabase connected! Processing query...\n")

    # Find all tables containing "url$" column
    try:
        db_cur.execute("""
            select t.table_name
            from information_schema.tables t
            inner join information_schema.columns c on c.table_name = t.table_name 
                                            and c.table_schema = t.table_schema
            where c.column_name = 'url$'
                and t.table_schema not in ('information_schema', 'pg_catalog')
                and t.table_type = 'BASE TABLE'
        """)
        benchling_tables = db_cur.fetchall()
        benchling_tables = [i[0] for i in benchling_tables]
    except:
        return BenchlingDbStatus.TABLES_NOT_FOUND

    query = """
        select id, url$ 
        from TABLE_NAME
    """
    unioned_query = ""

    for table_name in benchling_tables:
        unioned_query += query.replace("TABLE_NAME", table_name)
        if table_name != benchling_tables[-1]:
            unioned_query += "union\n"

    url_query = """
        select url$ 
        from ({}) as u
        where u.id = '{}';
    """.format(unioned_query, seq_id)

    print(url_query)

    try:
        db_cur.execute(url_query)
        result = db_cur.fetchall()
    except:
        return BenchlingDbStatus.QUERY_ERROR, None
    
    db_conn.close()

    if result:
        return BenchlingDbStatus.SUCCESS, result[0][0]
    else:
        return BenchlingDbStatus.URL_NOT_FOUND, None
