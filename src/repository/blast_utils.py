import os
from enum import Enum
import psycopg
from Bio import SeqIO
from ..global_settings import *


class DatabaseStatus(str, Enum):
    SUCCESS = "Success"
    CONNECTION_ERROR = "Postgres Connection Error"
    EXPORT_ERROR = "FASTA Export Error"
    IN_PROGRESS = "In Progress"
    NO_ENV_VAR = "Environmental Variables For DB Logins Are Missing"
    DIR_NOT_FOUND = f"'{SHARED_VOLUME}' is not a directory"


def save_benchling_db_to_fasta(fasta_path):
    '''
    Export DNA sequences from a Benchling warehouse database to a fasta file

            Paraterers:
                fasta_path (str): full path (including filename) for the
                                  output .fasta file
            
            Returns:
                status (str Enum): status code of the operation.
                                on success, returns DatabaseStatus.SUCCESS
    '''

    def clean_str(input_str):
        to_remove = ['"', 
                     '?column?',]
        input_str = str(input_str, 'utf8')
        for char in to_remove:
            input_str = input_str.replace(char, "")
        return input_str

    print("\nConnecting to database...\n")
    print(BENCHLING_POSTGRES_HOST + ":" + str(BENCHLING_POSTGRES_PORT))

    if not BENCHLING_POSTGRES_USER or not BENCHLING_POSTGRES_PASSWORD:
        return DatabaseStatus.NO_ENV_VAR

    try:
        db_conn = psycopg.connect(host=BENCHLING_POSTGRES_HOST,
                            port=BENCHLING_POSTGRES_PORT,
                            dbname=BENCHLING_POSTGRES_DATABASE,
                            user=BENCHLING_POSTGRES_USER,
                            password=BENCHLING_POSTGRES_PASSWORD)
        db_cur = db_conn.cursor()
    except:
        return DatabaseStatus.CONNECTION_ERROR

    print("\nDatabase connected! Processing query...\n")

    # Find all tables containing "schema" column
    # this is to add the schema type to each sequence in the FASTA file
    try:
        db_cur.execute("""
            select t.table_name
            from information_schema.tables t
            inner join information_schema.columns c on c.table_name = t.table_name 
                                            and c.table_schema = t.table_schema
            where c.column_name = 'schema'
                and t.table_schema not in ('information_schema', 'pg_catalog')
                and t.table_type = 'BASE TABLE'
        """)
        benchling_tables = [i[0] for i in db_cur.fetchall()]
    except:
        return DatabaseStatus.EXPORT_ERROR

    unioned_query = ""
    for table_name in benchling_tables:
        unioned_query += f"""
            select id, schema
            from {table_name}
        """
        if table_name != benchling_tables[-1]:
            unioned_query += "union\n"

    # Get id, name, schema type and sequence from the unioned tables
    # and format as FASTA 
    fasta_query = f"""
        select ('>' || regexp_replace(u.name, '[^[:ascii:]]', '', 'g') || ' ' || u.id || ' ' || u.schema || E'\n' || u.bases) 
        from
        (
            select {BENCHLING_POSTGRES_MAIN_TABLE}.id, 
                   {BENCHLING_POSTGRES_MAIN_TABLE}.name, 
                   {BENCHLING_POSTGRES_MAIN_TABLE}.bases, 
                   t.schema
            from "{BENCHLING_POSTGRES_MAIN_TABLE}" left join
            ({unioned_query}) as t
            on {BENCHLING_POSTGRES_MAIN_TABLE}.id = t.id 
        ) as u
        where u.id in (select id from "registry_entity$raw" rer)
    """

    output_query = "COPY ({}) TO STDOUT WITH CSV HEADER".format(fasta_query)

    try:
        with db_cur.copy(output_query) as copy:
            with open(fasta_path, 'w') as f:
                while data := copy.read():
                    f.write(clean_str(data))
    except:
        return DatabaseStatus.EXPORT_ERROR

    print("Query result has been saved to " + fasta_path)

    db_conn.close()

    return DatabaseStatus.SUCCESS


def make_blast_db(db_path):
    ''' Run makeblastdb command for all files in the provided directory '''
    for file in os.listdir(db_path):
        if file.lower().endswith(".fasta"):
            file_path = os.path.join(db_path, file)
            os.system("makeblastdb -in {} -dbtype nucl".format(file_path))


def is_fasta(file_path):
    ''' Check if a file is a valid FASTA file '''
    if not file_path.lower().endswith(".fasta"):
        return False
    with open(file_path, "r") as handle:
        try:
            fasta = SeqIO.parse(handle, "fasta")
            return any(fasta)
        except:
            return False


def run_query_and_build_db():
    '''
    Pulls query from Benchling warehouse, converts and saves dna sequences
    to a fasta file, validate all fasta files in the shared directory,
    and produce BLAST database files
    '''
    out_folder = SHARED_VOLUME
    out_file = os.path.join(SHARED_VOLUME, BENCHLING_DB_OUTPUT)
    if not os.path.isdir(out_folder):
        return DatabaseStatus.DIR_NOT_FOUND
    query_status = save_benchling_db_to_fasta(out_file)
    if query_status == DatabaseStatus.SUCCESS:
        make_blast_db(out_folder)
    return query_status
