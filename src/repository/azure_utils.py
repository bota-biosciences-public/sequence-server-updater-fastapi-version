from enum import Enum
from azure.identity import ClientSecretCredential
from azure.mgmt.web import WebSiteManagementClient

from ..global_settings import *


class AzureAppStatus(str, Enum):
    SUCCESS = "Success"
    FAILED = "Failed"
    PENDING = "Pending"
    NO_ENV_VAR = "Environmental Variables For Azure Credentials Are Missing"
    DID_NOT_RUN = "Did not restart the Azure App"


def restart_azure_app(credentials: ClientSecretCredential = None):
    if not credentials:
        credentials = ClientSecretCredential(
            client_id=AZURE_CLIENT_ID,
            client_secret=AZURE_SECRET,
            tenant_id=AZURE_TENANT
        )
    elif any(var is None for var in [AZURE_SUBSCRIPTION_ID, 
                                     AZURE_CLIENT_ID, 
                                     AZURE_SECRET, 
                                     AZURE_TENANT]):
        return AzureAppStatus.NO_ENV_VAR
    
    try:
        web_client = WebSiteManagementClient(credentials, AZURE_SUBSCRIPTION_ID)
        web_client.web_apps.restart(AZURE_RESOURCE_GROUP, AZURE_APP_SERVICE_NAME)
        return AzureAppStatus.SUCCESS
    except:
        return AzureAppStatus.FAILED
