from fastapi.templating import Jinja2Templates
from ..global_settings import *


templates = Jinja2Templates(directory="templates")


# Name of environmental variables this app uses
REQUIRED_VARS = ['SHARED_VOLUME',
                 'BENCHLING_DB_OUTPUT',
                 'BENCHLING_POSTGRES_USER',
                 'BENCHLING_POSTGRES_PASSWORD',
                 'BENCHLING_POSTGRES_HOST',
                 'BENCHLING_POSTGRES_PORT',
                 'BENCHLING_POSTGRES_DATABASE',
                 'BENCHLING_POSTGRES_MAIN_TABLE']
OPTIONAL_VARS = ['AZURE_SUBSCRIPTION_ID',
                 'AZURE_CLIENT_ID',
                 'AZURE_SECRET',
                 'AZURE_TENANT',
                 'AZURE_RESOURCE_GROUP',
                 'AZURE_APP_SERVICE_NAME']


def diagnose(request):
    ''' Check the existence of required & optional global configs '''
    
    global_vars = {"required": {}, "optional": {}}

    # It's much safer to use globals()['var_name] to test the existence of
    # a global variable than using eval()
    for var in REQUIRED_VARS:
        if var in globals() and globals()[var]:
            global_vars["required"][var] = "Found"
        else:
            global_vars["required"][var] = "***Missing***"

    for var in OPTIONAL_VARS:
        if var in globals() and globals()[var]:
            global_vars["optional"][var] = "Found"
        else:
            global_vars["optional"][var] = "***Missing***"

    shared_volume_mounted = False
    if "SHARED_VOLUME" in globals() and globals()["SHARED_VOLUME"]:
        if os.path.isdir(SHARED_VOLUME):
            shared_volume_mounted = True
    
    return templates.TemplateResponse("diagnostic.html", 
                                      {"request": request,
                                       "global_vars": global_vars,
                                       "volume_mounted":shared_volume_mounted})
