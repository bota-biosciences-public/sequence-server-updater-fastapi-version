import shutil
from sqlalchemy.orm import Session
from .. import models
from fastapi.templating import Jinja2Templates
from . import blast_utils, azure_utils
from .blast_utils import DatabaseStatus
from .azure_utils import AzureAppStatus
from ..global_settings import *


templates = Jinja2Templates(directory="templates")


def job_running(db: Session):
    prev_job = db.query(models.TaskStatus).order_by(-models.TaskStatus.id).first()
    if not prev_job or prev_job.db_status == DatabaseStatus.SUCCESS:
        print("No job running.")
        return False
    elif prev_job.db_status == DatabaseStatus.IN_PROGRESS: 
        print("Current update job running.")
        return True
    else:
        print("Current update job failed.")
        return False


def homepage(request, db: Session):
    all_status = db.query(models.TaskStatus).order_by(-models.TaskStatus.id).all()
    prev_job = job_running(db)
    return templates.TemplateResponse("home.html", {"request": request,
                                                    "all_status": all_status,
                                                    "prev_job": prev_job})


def run_query_and_build_db(db: Session, restart: bool):
    new_status = db.query(models.TaskStatus).order_by(-models.TaskStatus.id).first()
    query_status = blast_utils.run_query_and_build_db()
    new_status.db_status = query_status
    db.commit()

    if query_status == DatabaseStatus.SUCCESS and restart:
        azure_status = azure_utils.restart_azure_app()
        new_status.cloud_app_status = azure_status
    else:
        new_status.cloud_app_status = AzureAppStatus.DID_NOT_RUN
    
    db.commit()
    

def upload_fasta(request):
    return templates.TemplateResponse("upload.html", {"request": request})


def process_new_fasta(request, form_data):
    output_filename = os.path.join(SHARED_VOLUME, form_data.file.filename)
    with open(output_filename, "wb") as fasta:
        shutil.copyfileobj(form_data.file.file, fasta)

    file_is_valid = blast_utils.is_fasta(output_filename)
    if file_is_valid:
        blast_utils.make_blast_db(SHARED_VOLUME)
        message = "File imported successfully!"
    else:
        os.remove(output_filename)
        message = "Invalid FASTA file!"
    
    return templates.TemplateResponse("upload.html", {"request": request,
                                                      "message": message})

def restart_sequenceserver(request):
    status = azure_utils.restart_azure_app()
    success = True if status == azure_utils.AzureAppStatus.SUCCESS else False
    return templates.TemplateResponse("restart_result.html", 
                                      {"request": request,
                                       "success": success,
                                       "status": status})
