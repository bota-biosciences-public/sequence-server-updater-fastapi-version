from fastapi import APIRouter, Request
from fastapi.responses import HTMLResponse
from ..repository import diagnostic


router = APIRouter(
    tags=['Redirect']
)


@router.get('/diagnostic', response_class=HTMLResponse)
def diagnose(request: Request):
    return diagnostic.diagnose(request)
