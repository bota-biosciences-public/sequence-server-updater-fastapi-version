from fastapi import (
    APIRouter, 
    Depends,
    Request,
    BackgroundTasks,
    Form
)
from fastapi.responses import HTMLResponse
from sqlalchemy.orm import Session
from .. import database, models
from ..repository import updater
from ..repository.blast_utils import DatabaseStatus
from ..schemas import UploadFastaForm


router = APIRouter(
    tags=['Updater']
)

get_db = database.get_db


@router.get('/', response_class=HTMLResponse)
def homepage(request: Request, db: Session = Depends(get_db)):
    return updater.homepage(request, db)


@router.post('/', response_class=HTMLResponse)
def update_db(request: Request, 
                    background_tasks: BackgroundTasks,
                    db: Session = Depends(get_db),
                    restart: bool = Form(False),
                    ):
    if not updater.job_running(db):
        print("Starting new update job!")
        new_status = models.TaskStatus(db_status=DatabaseStatus.IN_PROGRESS)
        db.add(new_status)
        db.commit()
        db.refresh(new_status)
        background_tasks.add_task(updater.run_query_and_build_db, db, restart)
    return homepage(request, db)


@router.get('/upload_fasta', response_class=HTMLResponse)
def upload_fasta(request: Request):
    return updater.upload_fasta(request)


@router.post('/upload_fasta', response_class=HTMLResponse)
def upload_fasta(request: Request, 
                form_data: UploadFastaForm = Depends(UploadFastaForm.as_form)):
    return updater.process_new_fasta(request, form_data)


@router.post('/restart_app', response_class=HTMLResponse)
def restart_sequenceserver(request: Request):
    return updater.restart_sequenceserver(request)
