from fastapi import APIRouter, Request
from fastapi.responses import HTMLResponse
from starlette.responses import RedirectResponse
from ..repository.redirect_utils import get_benchling_url
from fastapi.templating import Jinja2Templates


router = APIRouter(
    tags=['Redirect']
)

templates = Jinja2Templates(directory="templates")


@router.get("/redirect/{seq_id}")
def redirect_to_benchling(seq_id: str):
    status, benchling_url = get_benchling_url(seq_id)
    if benchling_url:
        return RedirectResponse(url=benchling_url)
    else:
        return HTMLResponse(status)


@router.get("/redirect")
def redirect_instructions(request: Request):
    return templates.TemplateResponse("redirect_instructions.html", 
                                      {"request": request,})
