import os


"""
This file contains all the global configs for SequenceServer Database Updater
For more information, see README.md
"""


# This directory should be shared between this App and your SequenceServer app
# Default for SequenceServer is /db
SHARED_VOLUME = "/db"
# The output filename of the FASTA file generated from your Benchling Database
BENCHLING_DB_OUTPUT = "benchling_default.fasta"


# **REQUIRED**
# The login details of your Benchling Database
# You can use Environmental Variables for sensitive stuff like
# BENCHLING_POSTGRES_USER and BENCHLING_POSTGRES_PASSWORD
BENCHLING_POSTGRES_USER = os.getenv('BENCHLING_POSTGRES_USER')
BENCHLING_POSTGRES_PASSWORD = os.getenv('BENCHLING_POSTGRES_PASSWORD')
BENCHLING_POSTGRES_HOST = os.getenv('BENCHLING_POSTGRES_HOST')
BENCHLING_POSTGRES_PORT = 5432


# **REQUIRED**
# The Benchling database table from which the FASTA file will be generated
BENCHLING_POSTGRES_DATABASE = 'warehouse'
BENCHLING_POSTGRES_MAIN_TABLE = 'dna_sequence'


# **OPTIONAL**
# Set these env variables if your SequenceServer is deployed
# on Azure App Service, and you want to be able to restart it
# from the SequenceServer Updater web interface
# You can set these to None unless necessary
AZURE_SUBSCRIPTION_ID = os.getenv("AZURE_SUBSCRIPTION_ID")
AZURE_CLIENT_ID = os.getenv("AZURE_CLIENT_ID")
AZURE_SECRET = os.getenv("AZURE_SECRET")
AZURE_TENANT = os.getenv("AZURE_TENANT")
AZURE_RESOURCE_GROUP = os.getenv("AZURE_RESOURCE_GROUP")
AZURE_APP_SERVICE_NAME = os.getenv("AZURE_APP_SERVICE_NAME")
