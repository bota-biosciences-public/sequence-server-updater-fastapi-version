from fastapi import File, UploadFile
from pydantic import BaseModel


class UploadFastaForm(BaseModel):
    file: UploadFile

    @classmethod
    def as_form(
        cls, 
        file: UploadFile = File(...)
    ):
        return cls(file=file)
