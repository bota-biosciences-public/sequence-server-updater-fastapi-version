# Using NCBI's docker image to get the makeblastdb.
FROM ncbi/blast:2.12.0 AS ncbi-blast

# # Use python 3 as the base image.
FROM python:3.10.4-slim-buster

# Install packages required to run BLAST.
RUN apt-get update && apt-get install -y --no-install-recommends \
    curl libgomp1 liblmdb0 && rm -rf /var/lib/apt/lists/*

# Copy BLAST+ binaries.
COPY --from=ncbi-blast /blast/lib /blast/lib/
COPY --from=ncbi-blast /blast/bin/makeblastdb /blast/bin/makeblastdb

# Add BLAST+ binaries to PATH.
ENV PATH=/blast/bin:${PATH}

# Install GCC & G++ (Required by BioPython)
RUN apt-get update && apt-get install -y --no-install-recommends \
    gcc g++

# Run FastAPI App
WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD ["uvicorn", "src.main:app", "--host", "0.0.0.0"]

EXPOSE 8000 8000