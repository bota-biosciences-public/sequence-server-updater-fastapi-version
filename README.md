# SequenceServer Database Updater

SequenceServer Database Updater is a Web App for generating BLAST database from our own internal Benchling database. It will also automatically restart a SequenceServer (https://sequenceserver.com/) instance on Azure App Service. 


A few environmental variables need to be set before running the SequenceServer Database Updater:
- BENCHLING_POSTGRES_HOST // Address of your Benchling Warehouse Postgres Database
- BENCHLING_POSTGRES_USER   // User name for your Benchling Warehouse Postgres Database
- BENCHLING_POSTGRES_PASSWORD   // Password for your Benchling Warehouse Postgres Database
- AZURE_SUBSCRIPTION_ID   // (Optional) Azure subscription id of your SequenceServer instance on Azure App Service, if you want to enable automatic restart of SequenceServer
- AZURE_RESOURCE_GROUP // (Optional) Azure resource group of your SequenceServer instance
- AZURE_APP_SERVICE_NAME // (Optional) Name of your Azure App Service that hosts your SequenceServer instance

The following environmental variables has to be generated with Azure CLI with this command:
```bash
az ad sp create-for-rbac --role owner --scopes /subscriptions/YOUR_APP_SERVICE_SUBSCRIPTION_ID
```
- AZURE_CLIENT_ID  // (Optional)
- AZURE_SECRET   // (Optional)
- AZURE_TENANT   // (Optional)

## Deploy

After deploying on Azure App Service, mount a [Azure File Share](https://docs.microsoft.com/en-us/azure/storage/files/storage-how-to-create-file-share?tabs=azure-portal) to:
```
/db
```
of both the ***SequenceServer Database Updater*** and the ***SequenceServer*** instance.

**For detailed instructions on how to deploy this project on Azure, read the [complete guide in docs/azure_tutorial.md](docs/azure_tutorial.md).**
